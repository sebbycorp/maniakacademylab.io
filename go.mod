module maniakacademy

go 1.20

require (
	github.com/colinwilson/lotusdocs v0.1.0 // indirect
	github.com/gohugoio/hugo-mod-bootstrap-scss/v5 v5.20300.20200 // indirect
	github.com/maniak-academy/workshop-theme v0.0.0-20240109032421-b898c81ef177 // indirect
)
