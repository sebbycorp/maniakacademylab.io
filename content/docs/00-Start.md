---
weight: 2
title: "Start"
description: ""
icon: "article"
date: "2023-12-22T14:06:28-05:00"
lastmod: "2023-13-22T14:06:28-05:00"
draft: false
toc: true
---

## How to deploy VSCode

To test this demo locally all you need to do is spin up these 2 docker service examples to give you vscode and terminal.

* Docker installed on your local machine 
* Create a file called docker-compose.yaml
* Open terminal and execute ```docker-compose up -d```
* Note to stop them just run ```docker-compose-down```

Here is the docker-compose.yaml file


```
version: '3.8'

services:
  vs-code:
    image: sebbycorp/xademy-vscode:latest
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Etc/UTC
      - DEFAULT_WORKSPACE=/workspace/code #optional
    ports:
      - "8080:8080"
      
  terminal-server:
    image: sebbycorp/terminal-server:latest
    ports:
      - "7681:7681"
    environment:
      - TINI_KILL_PROCESS_GROUP=1
    # volumes:
    #   - /your/local/workspace:/workspace
    entrypoint: ["/sbin/tini", "--"]
    command: ["ttyd", "-s", "3", "-t", "titleFixed=/bin/sh", "-t", "rendererType=webgl", "-t", "disableLeaveAlert=true", "/bin/sh", "-i", "-l"]
```


...